# Instrumentality-facade
-- The frontend for the Instrumentality project --

## Requirements:
1. Install yarn from [https://yarnpkg.com/lang/en/](https://yarnpkg.com/lang/en/)
2. Install @angular/cli globally through yarn: `yarn global add @angular/cli`
3. Now type `ng config -g cli.packageManager yarn`. This will configure the angular cli to use yarn instead of npm

## How to build this part as a standalone project:
1. In a terminal clone this repo: `git clone https://github.com/alexandru-balan/instrumentality-facade.git`
2. Move to the cloned directory: `cd instrumentality-facade`
3. Run `yarn install` to install all of the dependencies of this project
4. Type `ng serve` to compile the project. The link at which the frontend will be available will pop up on the screen after compilation has finished
