import { Component, OnInit } from '@angular/core';

declare var VANTA: any;

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.less']
})
export class FeaturesComponent implements OnInit {
  effect: any;

  constructor() { }

  ngOnInit() {
    this.effect = VANTA.NET({
      el: "#net-background",
      color: 0xca6c56,
      backgroundColor: 0x342071,
      points: 10.00,
      maxDistance: 15.00,
      spacing: 20.00
  })
  }

  ngOnDestroy() {
    this.effect.destroy();
  }

}
