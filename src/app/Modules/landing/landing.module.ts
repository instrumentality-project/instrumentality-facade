import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { Routes, RouterModule } from "@angular/router";
import { BannerComponent } from './banner/banner.component';
import { FooterComponent } from './footer/footer.component';
import { PricingComponent } from './pricing/pricing.component';
import { FeaturesComponent } from './features/features.component';
import { LandingPageComponent } from './landing-page/landing-page.component';

const routes: Routes = [
  {
    path: '',
    component: LandingPageComponent,
    children: [
      {
        path: 'banner',
        component: BannerComponent
      },

      {
        path: 'pricing',
        component: PricingComponent
      },

      {
        path: 'features',
        component: FeaturesComponent
      },

      {
        path: '',
        redirectTo: 'banner',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  declarations: [NavbarComponent, BannerComponent, FooterComponent, PricingComponent, FeaturesComponent, LandingPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    LandingPageComponent
  ]
})
export class LandingModule { }
